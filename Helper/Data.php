<?php


namespace Avanti\Ebit\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    protected $store;

    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        return parent::__construct($context);
    }

    /**
     * @todo Get config value in config data
     * @param $path
     * @return mixed
     */
    public function getConfigValue($path)
    {
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE);
    }
}
